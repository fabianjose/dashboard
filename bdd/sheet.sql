-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2022 a las 23:51:31
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sheet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facebook`
--

CREATE TABLE `facebook` (
  `id` int(50) NOT NULL,
  `canal` varchar(255) NOT NULL,
  `segmento` varchar(255) NOT NULL,
  `n_caso` varchar(255) NOT NULL,
  `nit_cedula` varchar(255) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `ser_o_cus_servicio` varchar(255) NOT NULL,
  `fecha` date NOT NULL DEFAULT current_timestamp(),
  `municipio_departamento` varchar(255) NOT NULL,
  `motivo_caso` varchar(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `subcategoria` varchar(255) NOT NULL,
  `nombre_auditor` varchar(255) NOT NULL,
  `ag_monitoreado` varchar(255) NOT NULL,
  `fecha_monitoreo` date NOT NULL DEFAULT current_timestamp(),
  `protocolo_aten_vocacion_servicio` varchar(255) NOT NULL,
  `ag_uso_prot_salud` varchar(255) NOT NULL,
  `obs_ag_uso_prot_salud` varchar(255) NOT NULL,
  `ag_indaga_nombre_real_cliente` varchar(255) NOT NULL,
  `obs_ag_indaga_nombre_real_cliente` varchar(255) NOT NULL,
  `ag_dirije_cliente_nombre` varchar(255) NOT NULL,
  `obs_ag_dirije_cliente_nombre` varchar(255) NOT NULL,
  `ag_uso_prot_cont_inac_desp` varchar(255) NOT NULL,
  `obs_ag_uso_prot_cont_inac_desp` varchar(255) NOT NULL,
  `ortografia_redaccion` varchar(255) NOT NULL,
  `ag_redaccion_correcta` varchar(255) NOT NULL,
  `obs_ag_redaccion_correcta` varchar(255) NOT NULL,
  `ag_ortografiia_correcta` varchar(255) NOT NULL,
  `obs_ag_ortografiia_correcta` varchar(255) NOT NULL,
  `estructura_conversacion` varchar(255) NOT NULL,
  `orden_logico_conv` varchar(255) NOT NULL,
  `obs_orden_logico_conv` varchar(255) NOT NULL,
  `actitud_servicio_amabilidad` varchar(255) NOT NULL,
  `genera_maltrato_cliente` varchar(255) NOT NULL,
  `obs_genera_maltrato_cliente` varchar(255) NOT NULL,
  `ag_genera_empatia_comunicacion` varchar(255) NOT NULL,
  `obs_ag_genera_empatia_comunicacion` varchar(255) NOT NULL,
  `analisis_gestion_oportuna` varchar(255) NOT NULL,
  `rea_diagnostico_actitud_cliente` varchar(255) NOT NULL,
  `obs_rea_diagnostico_actitud_cliente` varchar(255) NOT NULL,
  `gest_realiz_linea_tipo_solicitud` varchar(255) NOT NULL,
  `obs_gest_realiz_linea_tipo_solicitud` varchar(255) NOT NULL,
  `ag_inter_oport_contact_red_soc` varchar(255) NOT NULL,
  `obs_ag_inter_oport_contact_red_soc` varchar(255) NOT NULL,
  `manejo_tiempo_espera` varchar(255) NOT NULL,
  `obs_manejo_tiempo_espera` varchar(255) NOT NULL,
  `entrega_inf_comp_corr_producotos` varchar(255) NOT NULL,
  `obs_entrega_inf_comp_corr_producotos` varchar(255) NOT NULL,
  `uso_herr_sistem_informacion` varchar(255) NOT NULL,
  `manejo_app_bus_st_sales` varchar(255) NOT NULL,
  `obs_manejo_app_bus_st_sales` varchar(255) NOT NULL,
  `ag_crea_tk_correcto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pqr`
--

CREATE TABLE `pqr` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pqr`
--

INSERT INTO `pqr` (`id`, `fecha`) VALUES
(1, '2022-09-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `name`, `rol`, `pass`) VALUES
(1, 'fabianjose', 'fabianjose', 'admin', '$2a$08$BeYOfQ6iNVtW/J2.0VCyU.zPkDklWwrnweF193otSKCf7x7USqFBe');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `facebook`
--
ALTER TABLE `facebook`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pqr`
--
ALTER TABLE `pqr`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facebook`
--
ALTER TABLE `facebook`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pqr`
--
ALTER TABLE `pqr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
