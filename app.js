// 1 - Invocamos a Express
const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const morgan = require('morgan');


// llamamos a morgan
//app.use(morgan('combined'))
//app.use(morgan('tiny'));
app.use(bodyparser.urlencoded({extended:true}))
app.use(bodyparser.json())
//3- Invocamos a rutas
app.use( require('./routes/index'))
//4 -seteamos el directorio de assets
app.use('/resources',express.static('public'));
app.use('/resources', express.static(__dirname + '/public'));

//5 - Establecemos el motor de plantillas
app.set('view engine','ejs');
//app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

/*

app.listen(3000, (req, res)=>{
    console.log('SERVER RUNNING IN http://localhost:3000');
});*/

app.listen(process.env.PORT || 3000, function() {
	console.log('Express app running on port ' + (process.env.PORT || 3000))
});