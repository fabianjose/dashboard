var express = require('express');
var router = express.Router();
const rutasGlobales = require('../controller/index.controller')
const datosfacebook = require('../controller/facebook.controller')
const datospqr = require('../controller/pqr.controller')
const datoscontactcenter = require('../controller/contactcenter.controller')
const datoscsecontact = require('../controller/csecontact.controller')
const datoscsecorreo = require('../controller/csecorreo.controller')
const datosnoc = require('../controller/noc.controller')
const datossm = require('../controller/sm.controller')
const datosallianz = require('../controller/allianz.controller')
const datosateledisca = require('../controller/teledisca.controller')


let facebooksheet = require('../sheetcontroller/facebooksheet');


const multer = require('multer')
const path = require('path')



//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, 'public/images/refutes')     // './public/images/' directory name where save the file
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
 
var upload = multer({
    storage: storage
});








var  usuario


//3- Invocamos a dotenv
const dotenv = require('dotenv');
dotenv.config({ path: './env/.env'});

//2 - Para poder capturar los datos del formulario (sin urlencoded nos devuelve "undefined")
router.use(express.urlencoded({extended:false}));
router.use(express.json());//además le decimos a express que vamos a usar json
//6 -Invocamos a bcrypt
const bcrypt = require('bcryptjs');

//7- variables de session
const session = require('express-session');
router.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));


// 8 - Invocamos a la conexion de la DB
const connection = require('../database/db');

//9 - establecemos las rutas
	router.get('/login',(req, res)=>{
		res.render('login');
	})

	/*router.get('/register',verification,(req, res)=>{
		res.render('register');
	})*/

//10 - Método para la REGISTRACIÓN
router.post('/register', async (req, res)=>{
	const user = req.body.user;
	const name = req.body.name;
    const rol = req.body.rol;
	const pass = req.body.pass;
	const email = req.body.email;
	const sub_rol = req.body.sub_rol;

	
	let passwordHash = await bcrypt.hash(pass, 8);
    connection.query('INSERT INTO users SET ?',{user:user, name:name,email:email, rol:rol,sub_rol:sub_rol, pass:passwordHash}, async (error, results)=>{
        if(error){
            console.log(error);
        }else{            
			res.send( {
				alert: true,
				alertTitle: "Registration",
				alertMessage: "¡Successful Registration!",
				alertIcon:'success',
				showConfirmButton: false,
				timer: 1500,
				ruta: ''
			});
        }
	});
})



//11 - Metodo para la autenticacion
router.post('/auth', async (req, res)=> {
	const user = req.body.user;
	const pass = req.body.pass;    
    let passwordHash = await bcrypt.hash(pass, 8);
	if (user && pass) {
		connection.query('SELECT * FROM users WHERE user = ?', [user], async (error, results, fields)=> {
			if( results.length == 0 || !(await bcrypt.compare(pass, results[0].pass)) ) {    
				res.render('login', {
                        alert: true,
                        alertTitle: "Error",
                        alertMessage: "USUARIO y/o PASSWORD incorrectas",
                        alertIcon:'error',
                        showConfirmButton: true,
                        timer: false,
                        ruta: 'login'    
                    });
				
				//Mensaje simple y poco vistoso
                //res.send('Incorrect Username and/or Password!');				
			} else {         
				//creamos una var de session y le asignamos true si INICIO SESSION       
				req.session.loggedin = true;                
				req.session.name = results[0].name;
				req.session.rol = results[0].rol;
				req.session.email = results[0].email;
				req.session.sub_rol = results[0].sub_rol;
				res.render('login', {					
					alert: true,
					alertTitle: "Conexión exitosa",
					alertMessage: "¡LOGIN CORRECTO!",
					alertIcon:'success',
					showConfirmButton: false,
					timer: 1500,
					ruta: ''
				});        			
			}			
			res.end();
		});
	} else {	
		res.send('Please enter user and Password!');
		res.end();
	}
});

//12 - Método para controlar que está auth en todas las páginas
router.get('/', (req, res)=> {
	const usuario = req.session
	if (req.session.loggedin) {
		res.render('index',{
			usuario:usuario,			
			login: true,
					
		});		
	} else {
		res.render('login',{
			login:false,
			name:'Debe iniciar sesión',			
		});				
	}
	res.end();
});

function verification (req, res, next ){
	if (req.session.loggedin) {	
	return next() 
}

else {
	res.render('login',{
		login:false,
		name:'Debe iniciar sesión',			
	});	
	}



}



//función para limpiar la caché luego del logout
router.use(function(req, res, next) {
    if (!req.user)
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    next();
});

 //Logout
//Destruye la sesión.
router.get('/logout', function (req, res) {
	req.session.destroy(() => {
	  res.redirect('/') // siempre se ejecutará después de que se destruya la sesión
	})
});


router.get('/telediscareportesetb',verification, datosateledisca.graficastelediscaEtb);
router.get('/telediscareportestotalplay',verification, datosateledisca.graficastelediscaTotalplay);
router.get('/telediscareportes',verification, datosateledisca.telediscareportes);




router.get('/allianzreportes',verification, datosallianz.chanelallianz);
router.get('/gestionoutbound',verification, datosallianz.outbound);
router.get('/gestionsms',verification, datosallianz.sms);
router.get('/especiales',verification, datosallianz.especiales);




router.get('/chanel',verification, rutasGlobales.chanel);
router.get('/register',verification, rutasGlobales.register);


//router.get('/', datosfacebook.obtenerDatos);
//router.post('/addfacebookform',verification, datosfacebook.insertarFormFacebook);
//router.get('/facebookDetalles',verification,datosfacebook.obtenerDatosFacebook);
//router.get('/facebookDetalles',verification,datosfacebook.obtenerDatosFacebook);
//router.get('/facebookDetallesErrores',verification,datosfacebook.obtenerDatosFacebookErrores);
//router.get('/agregarinfofacebook',verification,datosfacebook.viewInsertarFormFacebook);
//router.post('/filtroFechaErrorresFacebook',verification,datosfacebook.filtrarPorfecha)
router.get('/graficasfacebook',verification,datosfacebook.graficasfacebook)
router.get('/prueba', datosfacebook.prueba);
//router.get('/mostrar', datosfacebook.mostrar);






//////////////FACEBOOK//////////////////////
router.get('/descargarcsvfacebook', datosfacebook.descargarcsvcompleto);
router.post('/descargarcsvporfechafacebook', datosfacebook.descargarcsvporfechafacebook);
router.get('/facebookreclamos',verification,datosfacebook.facebookreclamos)
router.get('/facebookreclamosupdate',verification,datosfacebook.facebookreclamosupdate)
//router.get('/guardarsheet',verification,datosfacebook.guardarsheet)
//router.post('/guardarsheet',verification,datosfacebook.guardarsheet)
router.post('/crearrefute',verification,upload.single('image1'),datosfacebook.crearrefute)
router.get('/editarrefute/:id',verification,datosfacebook.editarrefute) 
router.post('/actualizarrefute/:id',verification,upload.single('image2'),datosfacebook.actualizarrefute)  
router.get('/facebookdescargarrefute',verification,datosfacebook.descargarrefute) 








//////////////FACEBOOK//////////////////////


//////////////CONTACT CENTER////////////////
router.get('/graficascontactcenter',verification,datoscontactcenter.graficascontactcenter)
router.get('/contactcenterreclamos',verification,datoscontactcenter.contactcenterreclamos)

router.get('/contactcenterreclamosupdate',verification,datoscontactcenter.contactcenterreclamosupdate)
router.post('/contactcentereditarsheet',verification,datoscontactcenter.updateSheet)
router.post('/contactcentercrearrefute',verification,upload.single('image1'),datoscontactcenter.crearrefute)
router.get('/contactcentereditarrefute/:id',verification,datoscontactcenter.editarrefute) 
router.post('/contactcenteractualizarrefute/:id',verification,upload.single('image2'),datoscontactcenter.actualizarrefute)  
router.get('/descargarcsvcontactcenter', datoscontactcenter.descargarcsvcompletocontactcenter);
router.post('/descargarcsvporfechacontactcenter', datoscontactcenter.descargarcsvporfechacontactcenter);
//router.get('/contactcenterdescargarrefute/:id',verification,datoscontactcenter.descargarrefute) 
router.get('/contactcenterdescargarrefute',verification,datoscontactcenter.descargarrefute) 





//////////////CONTACT CENTER////////////////


//////////////CSECONTACT////////////////
router.get('/graficascsecontact',verification,datoscsecontact.graficascsecontact)
router.get('/csecontactreclamos',verification,datoscsecontact.csereclamos)
router.get('/csecontactreclamosupdate',verification,datoscsecontact.csereclamosupdate)
router.post('/cseeditarsheet',verification,datoscsecontact.updateSheet)
router.post('/csecontactcrearrefute',verification,upload.single('image1'),datoscsecontact.crearrefute)
router.get('/csecontacteditarrefute/:id',verification,datoscsecontact.editarrefute) 
router.post('/csecontactactualizarrefute/:id',verification,upload.single('image2'),datoscsecontact.actualizarrefute)  
router.get('/descargarcsvcsecontact', datoscsecontact.descargarcsvcompletocsecontact);
router.post('/descargarcsvporfechacsecontact', datoscsecontact.descargarcsvporfechacsecontact);
router.get('/csecontactdescargarrefute',verification,datoscsecontact.descargarrefute) 



//////////////CSECONTACT////////////////


//////////////CSECORREO////////////////
router.get('/graficascsecorreo',verification,datoscsecorreo.graficascsecorreo)
router.get('/csecorreoreclamos',verification,datoscsecorreo.reclamos)
router.get('/csecorreoreclamosupdate',verification,datoscsecorreo.reclamosupdate)
router.post('/csecorreoeditarsheet',verification,datoscsecorreo.updateSheet)
router.post('/csecorreocrearrefute',verification,upload.single('image1'),datoscsecorreo.crearrefute)
router.get('/csecorreoeditarrefute/:id',verification,datoscsecorreo.editarrefute) 
router.post('/csecorreoactualizarrefute/:id',verification,upload.single('image2'),datoscsecorreo.actualizarrefute)  
router.get('/descargarcsvcsecorreo', datoscsecorreo.descargarcsvcompletocsecorreo);
router.post('/descargarcsvporfechacsecorreo', datoscsecorreo.descargarcsvporfechacsecorreo);

router.get('/csecorreodescargarrefute',verification,datoscsecorreo.descargarrefute) 

//////////////CSECORREO////////////////

//////////////NOC////////////////
router.get('/graficasnoc',verification,datosnoc.graficasnoc)
router.get('/nocreclamos',verification,datosnoc.reclamos)
router.get('/nocreclamosupdate',verification,datosnoc.reclamosupdate)
router.post('/noceditarsheet',verification,datosnoc.updateSheet)
router.post('/noccrearrefute',verification,upload.single('image1'),datosnoc.crearrefute)
router.get('/noceditarrefute/:id',verification,datosnoc.editarrefute) 
router.post('/nocactualizarrefute/:id',verification,upload.single('image2'),datosnoc.actualizarrefute)  
router.get('/descargarcsvnoc', datosnoc.descargarcsvcompletonoc);
router.post('/descargarcsvporfechanoc', datosnoc.descargarcsvporfechanoc);

router.get('/nocdescargarrefute',verification,datosnoc.descargarrefute) 

//////////////NOC////////////////


//////////////SM////////////////
router.get('/graficassm',verification,verification,datossm.graficascsm)
router.get('/smreclamos',verification,datossm.reclamos)
router.get('/smreclamosupdate',verification,datossm.reclamosupdate)
router.post('/smeditarsheet',verification,datossm.updateSheet)
router.post('/smcrearrefute',verification,upload.single('image1'),datossm.crearrefute)
router.get('/smeditarrefute/:id',verification,datossm.editarrefute) 
router.post('/smactualizarrefute/:id',verification,upload.single('image2'),datossm.actualizarrefute) 
router.get('/descargarcsvcsm', datossm.descargarcsvcompletosm);
router.post('/descargarcsvporfechasm', datossm.descargarcsvporfechasm); 
router.get('/smdescargarrefute',verification,datossm.descargarrefute) 

//////////////SM////////////////

//////////////PQR////////////////
router.get('/graficaspqr',verification,datospqr.graficaspqr)
router.get('/pqrreclamos',verification,datospqr.reclamos)
router.get('/pqrreclamosupdate',verification,datospqr.reclamosupdate)
router.post('/pqreditarsheet',verification,datospqr.updateSheet)
router.post('/pqrcrearrefute',verification,upload.single('image1'),datospqr.crearrefute)
router.get('/pqreditarrefute/:id',verification,datospqr.editarrefute) 
router.post('/pqractualizarrefute/:id',verification,upload.single('image2'),datospqr.actualizarrefute)  
router.get('/descargarcsvpqr', datospqr.descargarcsvcompletopqr);
router.post('/descargarcsvporfechapqr', datospqr.descargarcsvporfechapqr);
router.get('/pqrdescargarrefute',verification,datospqr.descargarrefute) 


//////////////PQR////////////////











//router.post('/guardar', datosfacebook.registrarPrueba)





module.exports = router;